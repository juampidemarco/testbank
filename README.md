
# Test Bank




## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/juampidemarco/testbank.git
```

Go to the root directory of the project and run

```bash
  gradlew bootRun
```

by default it runs on **localhost:8080**



## Run Tests
Go to the root directory of the project and run
```
    gradlew test
```

## API Reference

#### User Sign-up

```http
  POST /api/sign-up
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `name` | `string` | Your API key |
| `email` | `string` | **Required**. Your API key |
| `password` | `string` | **Required**. Your API key |
| `phones` | `List of Phone` | Your API key |

Phone

| Parameter     | Type      | Description               |
| :--------     | :-------  | :-------------------------|
| `number`      | `string`  |  Your API key             |
| `citycode`    | `string`  |  Your API key             |
| `contrycode`  | `string`  |  Your API key             |

Request:
```json
curl --location --request POST 'localhost:8080/users/sign-up' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "juan pablo",
    "email": "juanpablo@gmail.com",
    "password": "12Abcdefg*",
    "phones": [
        {
        "number": 12345,
        "citycode": 1234,
        "countrycode": 1234
        }
    ]
}'
```
Response 201 CREATED:
```json
{
  "user": {
    "id": "042a10f2-abad-42e0-b503-dc1f6bd3e708",
    "name": "juan pablo",
    "email": "juanpablo@gmail.com",
    "password": "$2a$12$EL9dtAFrBBdMGI.S7hJHEuaqLXz5o7n27HSStLjyCAihrGlSlcvHq",
    "phones": [
      {
        "id": 1,
        "number": 12345,
        "citycode": 1234,
        "countrycode": 1234
      }
    ],
    "created": "2022-10-17T23:02:10.115",
    "lastLogin": "2022-10-17T23:02:10.115",
    "token": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqdWFucGFibG9AZ21haWwuY29tIiwiaWQiOiJqdWFucGFibG9AZ21haWwuY29tIiwiaWF0IjoxNjY2MDU4NTI5LCJleHAiOjE2NjYwNjIxMjl9.GyoVIFckNSG4hTby39gayPP0iv1_g8izgbAgHkk6C10",
    "active": true
  }
}
```

#### Get item

```http
  GET /api/login
```
Must add Authorization values(token obtained in sign-up endpoint) to header
Request:

```
curl --location --request GET 'localhost:8080/users/login' \
--header 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9...'
```

Response 200 OK:
```json
{
  "id": "042a10f2-abad-42e0-b503-dc1f6bd3e708",
  "created": "2022-10-17T23:02:10.115",
  "lastlogin": "2022-10-17T23:04:35.501",
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqdWFucGFibG9AZ21haWwuY29tIiwiaWF0IjoxNjY2MDU4Njc1LCJleHAiOjE2NjYwNTkyNzV9.xp1VZff7UMQJIk0uh3AGyK7MF4rJS5_ErFvjEZo3oK0-BouFeuQvIGMuTky4hF57Uu6DMxNuLLp_lhmQJ0V34Q",
  "name": "juan pablo",
  "email": "juanpablo@gmail.com",
  "password": "$2a$12$EL9dtAFrBBdMGI.S7hJHEuaqLXz5o7n27HSStLjyCAihrGlSlcvHq",
  "phones": [
    {
      "id": 1,
      "number": 12345,
      "citycode": null,
      "countrycode": null
    }
  ],
  "active": true
}
```


## Documentation

The diagrams are in the diagrams folder
