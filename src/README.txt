INSTRUCCIONES

- Ejecutar el proyecto desde la clase principal -> TestBankApplication
- Desde postman (se adjunta casos de prueba en: src -> main -> resources -> postman),
    ejecutar con metodo POST el endpoint ->  http://localhost:8080/api/sign-up con el siguiente formato de request:

        {
            "name": "juan pablo",
            "email": "juanpablo@gmail.com",
            "password": "12A45789abc*",
            "phones": [
                {
                    "number": 12345,
                    "citycode": 1234,
                    "countrycode": "1234"
                },
                {
                    "number": 67890,
                    "citycode": 4567,
                    "countrycode": "4567"
                }
                    ]
        }

ACLARACIONES:
        - Se valida que el usuario no exista
        - Formato de email y contraseña segun solicitado
        - Campos email y password obligatorios
        - Campos name y phones no obligatorios

- Consultar en la BD los datos generados y persistidos -> http://localhost:8080/h2-ui/
    - username = sa (sin password)

- Para el Login, consultar con metodo GET el endpoint -> http://localhost:8080/api/login?name=Juan
- QueryParams requeridos: email y password

OBSERVACIONES
- Se adjunta capturas de pantalla de los request y response del proyecto en la carperta:
    src -> main -> resources -> test_image
- Se adjunta video de la api funcionando en la carperta:
    src -> main -> resources -> demo
- Se adjunta diagrama de secuencia y componentes en carpeta:
    src -> main -> resources -> diagrams

