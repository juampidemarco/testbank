package com.gl.testBank.mapper;

import com.gl.testBank.dto.LoginResponseDTO;
import com.gl.testBank.dto.PhoneDTO;
import com.gl.testBank.dto.UserDTO;
import com.gl.testBank.model.Phone;
import com.gl.testBank.model.User;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class UserMapper {

    public User toUser(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(12); // Strength set as 12
        String encodedPassword = encoder.encode(userDTO.getPassword());
        user.setPassword(encodedPassword);
        user.setEmail(userDTO.getEmail());
        user.setActive(true);
        user.setLastLogin(LocalDateTime.now());
        user.setCreated(LocalDateTime.now());
        List<Phone> phones = userDTO.getPhones().stream()
                .map(phoneDto -> toPhone(phoneDto, user))
                .collect(Collectors.toList());
        user.setPhones(phones);

        return user;
    }

    private Phone toPhone(PhoneDTO phoneDTO, User user) {
        Phone phone = new Phone();
        phone.setCitycode(phoneDTO.getCitycode());
        phone.setCountrycode(phoneDTO.getCountryCode());
        phone.setNumber(phoneDTO.getNumber());
        phone.setUser(user);
        return phone;
    }

    public LoginResponseDTO createLoginResponse(User user, String token) {

        LoginResponseDTO response = new LoginResponseDTO();
        response.setId(user.getId());
        response.setCreated(user.getCreated());
        response.setLastlogin(user.getLastLogin());
        response.setToken(token);
        response.setActive(user.isActive());
        response.setName(user.getName());
        response.setEmail(user.getEmail());
        response.setPhones(user.getPhones());
        response.setPassword(user.getPassword());
        return response;
    }
}
