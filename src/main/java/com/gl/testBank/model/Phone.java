package com.gl.testBank.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
@Table(name = "phone")
public class Phone implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private Long number;
    private Integer citycode;
    private String countrycode;

    @JsonBackReference
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_id")
    private User user;


    @Override
    public String toString() {
        return "Phone [id=" + id + ", number=" + number + ", citycode=" + citycode + ", countrycode=" + countrycode + "]";
    }
}
