package com.gl.testBank.service;

public interface TokenService {
    String getEmailFromToken(String token);
    String generateToken(String token);
    String createToken(String email);
    boolean validateToken(String token);
}
