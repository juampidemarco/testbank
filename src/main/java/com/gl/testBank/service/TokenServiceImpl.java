package com.gl.testBank.service;

import com.gl.testBank.exception.TokenException;
import com.gl.testBank.util.JwtProvider;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TokenServiceImpl implements TokenService{

    private final JwtProvider jwtProvider;

    @Override
    public String getEmailFromToken(String token) {
        return jwtProvider.getEmailFromToken(token);
    }

    @Override
    public String generateToken(String token) {
        return jwtProvider.generateToken(token);
    }

    @Override
    public String createToken(String email) {
        return jwtProvider.createToken(email);
    }

    @Override
    public boolean validateToken(String token) {
        if(token == null || token.trim().isEmpty()) {
            throw new TokenException("Token can not be empty");
        }
        if (!jwtProvider.validate(token)){
            throw new TokenException("Invalid Token");
        }
        return true;
    }
}
