package com.gl.testBank.service;

import com.gl.testBank.dto.UserDTO;
import com.gl.testBank.model.Phone;
import com.gl.testBank.model.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User saveUser(UserDTO userDTO);

    User saveUser(User user);

    List<User> getUsersByName(String name);

    Optional<User> getUserByEmail(String username);

    List<Phone> getPhonesByUser(String id_user);

    boolean validFormatEmail(String email);

    boolean validFormatPassword(String password);

    boolean validateUser(UserDTO user);

}
