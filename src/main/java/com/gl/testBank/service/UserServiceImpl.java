package com.gl.testBank.service;

import com.gl.testBank.dto.UserDTO;
import com.gl.testBank.exception.ValidateException;
import com.gl.testBank.mapper.UserMapper;
import com.gl.testBank.model.Phone;
import com.gl.testBank.model.User;
import com.gl.testBank.repository.PhoneRepository;
import com.gl.testBank.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhoneRepository repositoryPhone;

    @Autowired
    private final UserMapper mapper;

    @Override
    public User saveUser(UserDTO userDTO) {
        User user = mapper.toUser(userDTO);
        return userRepository.save(user);
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getUsersByName(String name) {
        List<User> list = userRepository.findAll();
        return list.stream().filter(x -> x.getName().matches(name)).collect(Collectors.toList());
    }

    @Override
    public Optional<User> getUserByEmail(String username) {
        return userRepository.findByEmail(username);
    }

    @Override
    public List<Phone> getPhonesByUser(String id_user) {
        List<Phone> list = repositoryPhone.findAll();
        return list.stream().filter(x -> x.getUser().getId().toString().equals(id_user)).collect(Collectors.toList());
    }

    @Override
    public boolean validFormatEmail(String email) {
        String regx = "^[A-Za-z0-9+_.-]+@(.+)$";
        //Compile regular expression to get the pattern
        Pattern pattern = Pattern.compile(regx);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }


    @Override
    public boolean validateUser(UserDTO user) {
        //Valid User null
        if (Objects.isNull(user)) {
            throw new ValidateException("User input incorrect");
        }
        //Valid Password null
        if (Objects.isNull(user.getPassword())) {
            throw new ValidateException("Password is required");
        }
        //Valid Number Characters
        if (!checkNumberCharacters(user.getPassword())) {
            throw new ValidateException("Password must be between 8 and 12 characters");
        }
        //Valid Format Password
        if (!validFormatPassword(user.getPassword())) {
            throw new ValidateException("Incorrect format password");
        }
        //Valid Email null
        if (Objects.isNull(user.getEmail())) {
            throw new ValidateException("Email is required");
        }
        //Valid Email format
        if (!validFormatEmail(user.getEmail())) {
            throw new ValidateException("Incorrect format email");
        }
        //Valid name optional
        if (Objects.isNull(user.getName())) {
            user.setName("");
        }
        //Valid Phone optional
        if (Objects.isNull(user.getPhones())) {
            user.setPhones(new ArrayList<>());
        }
        //Valid if exists user
        if (!getUsersByName(user.getName()).isEmpty()) {
            throw new ValidateException("User already exists");
        }
        return true;
    }

    public boolean validFormatPassword(String password) {
        return checkCapitaLetter(password) && checkNumber(password);
    }

    private boolean checkCapitaLetter(String password) {
        String regx = "[A-Z]{1}";
        int count = 0;
        Pattern pattern = Pattern.compile(regx);
        Matcher matcher = pattern.matcher(password);

        while (matcher.find()) {
            count++;
        }
        return count == 1;
    }

    private boolean checkNumber(String password) {
        String regx = "[0-9]{1}";
        int count = 0;
        Pattern pattern = Pattern.compile(regx);
        Matcher matcher = pattern.matcher(password);

        while (matcher.find()) {
            count++;
        }
        return count == 2;
    }

    private boolean checkNumberCharacters(String password) {
        return (password.length() >= 8 && password.length() <= 12);
    }

}
