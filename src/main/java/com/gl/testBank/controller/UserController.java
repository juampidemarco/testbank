package com.gl.testBank.controller;

import com.gl.testBank.dto.LoginResponseDTO;
import com.gl.testBank.dto.UserDTO;
import com.gl.testBank.dto.UserResponseDTO;
import com.gl.testBank.usecase.UserUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class UserController {

    private final UserUseCase userUseCase;

    public UserController(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @PostMapping(value = "/sign-up", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserResponseDTO> signUp(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(userUseCase.signUp(userDTO), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LoginResponseDTO> login(HttpServletRequest request) {
        String token = getToken(request);
        LoginResponseDTO responseDTO = userUseCase.login(token);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    private String getToken(HttpServletRequest request){
        String header = request.getHeader("Authorization");
        if(header != null && header.startsWith("Bearer"))
            return header.replace("Bearer ", "");
        return null;
    }
}