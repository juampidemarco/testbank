package com.gl.testBank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {Exception.class})
    private ResponseEntity<Object> handleGenericException(Exception e) {
        ApiException apiException =
                new ApiException(new Date(System.currentTimeMillis()), HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        return new ResponseEntity<>(apiException, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {ValidateException.class})
    private ResponseEntity<Object> handleApiRequestException(ValidateException e) {
        ApiException apiException =
                new ApiException(new Date(System.currentTimeMillis()), HttpStatus.BAD_REQUEST.value(), e.getMessage());
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {TokenException.class})
    private ResponseEntity<Object> handleApiTokenException(TokenException e) {
        ApiException apiException =
                new ApiException(new Date(System.currentTimeMillis()), HttpStatus.UNAUTHORIZED.value(), e.getMessage());
        return new ResponseEntity<>(apiException, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {UserNotFoundException.class})
    private ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException e) {
        ApiException apiException =
                new ApiException(new Date(System.currentTimeMillis()), HttpStatus.NOT_FOUND.value(), e.getMessage());
        return new ResponseEntity<>(apiException, HttpStatus.NOT_FOUND);
    }
}
