package com.gl.testBank.usecase;


import com.gl.testBank.dto.LoginResponseDTO;
import com.gl.testBank.dto.UserDTO;
import com.gl.testBank.dto.UserResponseDTO;
import com.gl.testBank.exception.UserNotFoundException;
import com.gl.testBank.model.Phone;
import com.gl.testBank.model.User;
import com.gl.testBank.service.TokenService;
import com.gl.testBank.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Slf4j
@Service
public class UserUseCase {

    private final UserService userService;
    private final TokenService tokenService;


    public UserUseCase(UserService userService, TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    public UserResponseDTO signUp(UserDTO userDTO) {

        log.info("Validating request");
        userService.validateUser(userDTO);
        log.info("Request OK");

        log.info("Create token");
        String jwt = tokenService.createToken(userDTO.getEmail());

        log.info("Saving User: " + userDTO.getName());
        User user = userService.saveUser(userDTO);
        user.setToken(jwt);
        log.info("Save User: " + userDTO.getName() + " - OK");

        UserResponseDTO response = new UserResponseDTO();
        response.setUser(user);

        return response;
    }

    public LoginResponseDTO login(String token) {

        log.info("Validating token");
        tokenService.validateToken(token);
        log.info("Token OK");

        log.info("Get User by Token");
        String userEmail = tokenService.getEmailFromToken(token);

        Optional<User>  optionalUser = userService.getUserByEmail(userEmail);
        log.info("Match User: "+optionalUser.get().getName());

        LoginResponseDTO login = new LoginResponseDTO();

        if (optionalUser.isPresent()) {
            User userResponse = optionalUser.get();
            String newToken = tokenService.generateToken(userResponse.getEmail());
            userResponse.setToken(newToken);
            userResponse.setLastLogin(LocalDateTime.now());
            userService.saveUser(userResponse);

            login.setId(userResponse.getId());
            login.setCreated(userResponse.getCreated());
            login.setLastlogin(LocalDateTime.now());
            login.setToken(userResponse.getToken());
            login.setActive(true);
            login.setName(userResponse.getName());
            login.setEmail(userResponse.getEmail());
            login.setPassword(userResponse.getPassword());
            List<Phone> phones = userService.getPhonesByUser(userResponse.getId().toString());
            login.setPhones(phones);
            log.info("Get Login Response - OK");

        }else throw new UserNotFoundException("User Not Found");
        return login;
    }
}
