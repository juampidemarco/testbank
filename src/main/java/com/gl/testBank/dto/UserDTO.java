package com.gl.testBank.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.List;


@Getter
@Setter
public class UserDTO {

    private String name;

    @Email(regexp = ".+@.+\\..+", message = "Invalid Format")
    private String email;

    @Size(min = 8, max = 12, message = "Invalid Format")
    private String password;
    private List<@Valid PhoneDTO> phones;
}
