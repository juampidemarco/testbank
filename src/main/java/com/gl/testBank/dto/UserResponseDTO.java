package com.gl.testBank.dto;

import com.gl.testBank.model.User;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserResponseDTO {

    private User user;
}
