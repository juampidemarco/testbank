package com.gl.testBank.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneDTO {

    @NotNull
    private Long number;

    @NotNull
    @JsonProperty("city_code")
    private Integer citycode;

    @NotNull
    @JsonProperty("country_code")
    private String countryCode;
}
