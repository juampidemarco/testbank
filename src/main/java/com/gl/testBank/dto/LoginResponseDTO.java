package com.gl.testBank.dto;

import com.gl.testBank.model.Phone;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class LoginResponseDTO {

    private UUID id;
    private LocalDateTime created;
    private LocalDateTime lastlogin;
    private String token;
    private boolean isActive;
    private String name;
    private String email;
    private String password;
    private List<Phone> phones;
}
