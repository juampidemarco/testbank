package usecase

import com.gl.testBank.dto.UserDTO
import com.gl.testBank.model.User
import com.gl.testBank.service.TokenService
import com.gl.testBank.service.UserService
import com.gl.testBank.usecase.UserUseCase
import com.gl.testBank.util.JwtProvider
import spock.lang.Specification

import java.time.LocalDateTime

class UserUseCaseTest extends Specification{
    UserService userService
    TokenService tokenService
    JwtProvider jwtProvider
    UserUseCase userUseCase

    def setup() {
        userService = Mock(UserService)
        tokenService = Mock(TokenService)
        jwtProvider = Mock(JwtProvider)

        userUseCase = new UserUseCase(userService, tokenService)
    }


    def "login - return the user Ok" () {
        given:
        def token = "token"
        def newToken = "newToken"
        def userEmail = "juanpablo@mail.com"
        def encryptedPassword = "2a12EL9dtAFrBBdMGI"
        def user = new User()
        def now = new Date()
        def id = UUID.randomUUID()
        user.setId(id)
        user.setName("juan")
        user.setEmail(userEmail)
        user.setToken(token)
        user.setCreated(LocalDateTime.now())
        user.setLastLogin(LocalDateTime.now())
        user.setPassword(encryptedPassword)
        jwtProvider.validate(token) >> _
        jwtProvider.getEmailFromToken(token) >> userEmail
        userService.getUserByEmail(userEmail) >> Optional.of(user)
        jwtProvider.generateToken(user.getEmail()) >> newToken
        userService.saveUser(user) >> user
        tokenService.getEmailFromToken(token) >> userEmail
        when:
        def response = userUseCase.login(token)
        then:
        response.getEmail() == userEmail
    }

    def "should persist user if have a valid mail and password(other fields are optionals)" () {
        given:
        def userEmail = "juan@email.com"
        def password = "12Abcdefg*"
        def name = "juan"
        def id = UUID.randomUUID()
        def token = "token"

        userService.getUserByEmail(userEmail) >> Optional.empty()

        def userDTO = new UserDTO()
        userDTO.setName(name)
        userDTO.setEmail(userEmail)
        userDTO.setPassword(password)

        def user = new User()
        user.setId(id)
        user.setName("juan")
        user.setEmail(userEmail)
        user.setToken(token)
        user.setCreated(LocalDateTime.now())
        user.setLastLogin(LocalDateTime.now())
        user.setPassword(password)

        userService.saveUser(userDTO) >> user

        tokenService.createToken(userDTO.getEmail()) >> token

        when:
        def userResponse = userUseCase.signUp(userDTO)

        then:
        userResponse.getUser().getEmail() == user.getEmail()
    }


}
