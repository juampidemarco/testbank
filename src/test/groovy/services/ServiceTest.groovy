package services

import com.gl.testBank.model.User
import com.gl.testBank.service.UserService
import spock.lang.Specification

import java.util.regex.Matcher
import java.util.regex.Pattern

class ServiceTest extends Specification{

    private UserService service = Mock()

    def "email valid"() {
        given: "email with format correct"
        String email = "juan@gmail.com"
        String regx = "^[A-Za-z0-9+_.-]+@(.+)"

        when: "Compile regular expression to get the pattern"
        Pattern pattern = Pattern.compile(regx)
        Matcher matcher = pattern.matcher(email)

        then: "email format valid"
        matcher.matches() == true
    }

    def "email invalid"() {
        given: "email with format incorrect"
        String email = "juangmail.com"
        String regx = "^[A-Za-z0-9+_.-]+@(.+)"

        when: "Compile regular expression to get the pattern"
        Pattern pattern = Pattern.compile(regx)
        Matcher matcher = pattern.matcher(email)

        then: "email format invalid"
        matcher.matches() == false
    }

    def "Save User"() {
        given: "create user"
        User newUser = new User()
        newUser.setName("Juan")
        newUser.setEmail("juan@gmail.com")
        newUser.setPassword("A12345678b")
        List<User> list = service.findAll();

        def  service = Mock(UserService)

        when: "Save user"
        final User user = service.saveUser(newUser)

        then: "User ok"
        list.stream().filter(x -> x.getName().matches(newUser.getName()))
    }
}
